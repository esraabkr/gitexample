package beans;

import java.sql.*;
import javax.faces.bean.SessionScoped;
import org.hibernate.Session;

import javax.faces.bean.ManagedBean;
import java.util.Date;
import java.util.Properties;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.mail.internet.MimeMessage.RecipientType;
import sun.rmi.transport.Transport;

@ManagedBean
@SessionScoped
public class Data {

    private Members m;
    private HibernateUtil helper;
    private Session session;
    private String name;
    private String pass;
    private Date birth;
    private String country;
    private String email;
    private String f_name;
    private String l_name;
    private String photo;
    private String es;
    Connection con;
    Statement ps;
    ResultSet rs;
    private List perInfoAll = new ArrayList();

    public String getName() {
        session = helper.getSessionFactory().openSession();
        session.beginTransaction();
        m = (Members) session.get(Members.class, 16);
        this.name = m.getName();
        return name;
    }

    public String display(String name, String pass) {
        int i = 0;
        try {

            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://localhost:3306/user", "root", "");

            ps = con.createStatement();
            rs = ps.executeQuery("select * from members");
            while (rs.next()) {
                //  perInfoAll.add(i, new Members(rs.getString(1), rs.getString(2), rs.getDate(3), rs.getString(4), rs.getString(5), rs.getString(6), rs.getString(7), rs.getString(8)));
                if (rs.getString(name) == name && rs.getString(pass) == pass) {
                    String esraa = rs.getString(f_name);
                    String soso = rs.getString(l_name);
                    es=(esraa + " " + soso);
                    break;
                }else
                {
                    es=("sorry user name or password is incorrect");
                }
                i++;

            }

        } catch (Exception e) {
            System.out.println("Error Data : " + e.getMessage());
        }

        return "account.xhtml";

    }

    public String addMember(String name, String pass, Date birth, String country, String email, String f_name, String l_name, String photo) {
        m = new Members(name, pass, birth, country, email, f_name, l_name, photo);
        session = helper.getSessionFactory().openSession();
        session.beginTransaction();
        session.save(m);
        session.getTransaction().commit();
        session.close();
        return "verify.xhtml";

    }

    public String action() {
        return "login.xhtml";
    }

    public String action2() {
        return getEs();

    }

    /*  public void sendEmail() {
   inal String SMTP_HOST = "smtp.gmail.com";
        final String SMTP_PORT = "587";
        final String GMAIL_USERNAME = "xxxxxxxxxx@gmail.com";
        final String GMAIL_PASSWORD = "xxxxxxxxxx";

        System.out.println("Process Started");

        Properties prop = System.getProperties();
        prop.setProperty("mail.smtp.starttls.enable", "true");
        prop.setProperty("mail.smtp.host", SMTP_HOST);
        prop.setProperty("mail.smtp.user", GMAIL_USERNAME);
        prop.setProperty("mail.smtp.password", GMAIL_PASSWORD);
        prop.setProperty("mail.smtp.port", SMTP_PORT);
        prop.setProperty("mail.smtp.auth", "true");
        System.out.println("Props : " + prop);

        Session session = Session.getInstance(prop, new Authenticator() {
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication(GMAIL_USERNAME,
                        GMAIL_PASSWORD);
            }
        });

        System.out.println("Got Session : " + session);

        MimeMessage message = new MimeMessage(session);
        try {
            System.out.println("before sending");
            message.setFrom(new InternetAddress(GMAIL_USERNAME));
            message.addRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(GMAIL_USERNAME));
            message.setSubject("My First Email Attempt from Java");
            message.setText("Hi, This mail came from Java Application.");
            message.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(GMAIL_USERNAME));
            Transport transport = session.getTransport("smtp");
            System.out.println("Got Transport" + transport);
            transport.connect(SMTP_HOST, GMAIL_USERNAME, GMAIL_PASSWORD);
            transport.sendMessage(message, message.getAllRecipients());
            System.out.println("message Object : " + message);
            System.out.println("Email Sent Successfully");
        } catch (AddressException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MessagingException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }*/
    /**
     * @return the birth
     */
    public Date getBirth() {
        return birth;
    }

    /**
     * @param birth the birth to set
     */
    public void setBirth(Date birth) {
        this.birth = birth;
    }

    /**
     * @return the es
     */
    public String getEs() {
        return es;
    }

    /**
     * @param es the es to set
     */
    public void setEs(String es) {
        this.es = es;
    }

}
