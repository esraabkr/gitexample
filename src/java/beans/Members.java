package beans;

import java.util.Date;

public class Members {

    private int id;
    private String name;
    private String pass;
    private Date birth;
    private String country;
    private String email;
    private String f_name;
    private String l_name;
    private String photo;

    public Members() {

    }

    public Members(String name, String pass, Date birth, String country, String email, String f_name, String l_name, String photo) {
        this.name = name;
        this.pass = pass;
        this.birth = birth;
        this.country = country;
        this.email = email;
        this.f_name = f_name;
        this.l_name = l_name;
        this.photo = photo;

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public Date getBirth() {
        return birth;
    }

    public void setBirth(Date birth) {
        this.birth = birth;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getF_name() {
        return f_name;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public String getL_name() {
        return l_name;
    }

    public void setL_name(String l_name) {
        this.l_name = l_name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

}
